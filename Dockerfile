FROM python:3.9

ENV PYTHONUNBUFFERED 1

RUN pip install poetry==1.3.2 && poetry config virtualenvs.create false

WORKDIR /app
COPY pyproject.toml .
COPY poetry.lock .
RUN poetry install

COPY . /app/

EXPOSE 8000

WORKDIR /app/src

CMD python3 manage.py migrate && python3 manage.py runserver 0.0.0.0:8000
